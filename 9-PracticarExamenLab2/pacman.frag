#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform sampler2D colorMap;

in vec2 vtexCoord;

uniform float time;

bool boundings(vec2 vtex, vec2 boundX, vec2 boundY){
	bool x = vtex.x > boundX.x && vtex.x < boundX.y;
	bool y = vtex.y > boundY.x && vtex.y < boundY.y;
	return x && y;
}

vec2 getBounds(vec2 vtex){
	bool found = false;
	float x;
	for(float i = 0.5; !found && i <= 5; i+=0.5){
		if(vtex.x < i){
			x = i;
			found = true;
		}
	}
	float boundX = x-0.5;

	float y;
	found = false;
	for(float j = 1; !found && j <= 10; ++j){
		if(vtex.y < j){
			y = j;
			found = true;
		}
	}
	float boundY = y-1.0;
	return vec2(boundX,boundY);
}

vec2 getCenter(vec2 coords){
	return coords + vec2(0.25,0.5);
}

void main()
{
	vec4 col = vec4(0);
	int fantasma = -1;
	if(boundings(vtexCoord,vec2(2.5,3),vec2(3,4))) //Pacman
		col = texture(colorMap,vtexCoord);

	else if(boundings(vtexCoord,vec2(1.5,2),vec2(3,4))){ //Fantasma red
		col = texture(colorMap,vtexCoord+vec2(0.5,1));
		fantasma = 0;	
	}

	else if(boundings(vtexCoord,vec2(2.5,3),vec2(5,6))){ //Fantasma rose
		col = texture(colorMap,vtexCoord+vec2(0.5,1));
		fantasma = 1;
	}

	else if(boundings(vtexCoord,vec2(2.5,3),vec2(1,2))){ //Fantasma blue
		col = texture(colorMap,vtexCoord+vec2(0.5,1));
		fantasma = 2;	
	}

	else if(boundings(vtexCoord,vec2(3.5,4),vec2(3,4))){ //Fantasma taronja
		col = texture(colorMap,vtexCoord+vec2(0.5,1));
		fantasma = 3;	
	}
	
	else{
		vec2 bounds = getBounds(vtexCoord);
		vec2 center = getCenter(bounds);
		if(int((bounds.x)*2)%2==0 && int(bounds.y)%2==0){
			float dx = distance(center.x,vtexCoord.x);
			float dy = distance(center.y,vtexCoord.y);
			float d = step(0.2,dx) + step(0.4,dy);
			if(d == 0) col = vec4(0);
			else col = vec4(0,0.2,0.7,0);
		}
		else{
			float d = distance(center,vtexCoord);
			float num = 1-step(0.05,d);
			col = vec4(num,num,0,0);
		}
	}
	if(fantasma != -1){
		if(fantasma == 1){ //rosa bien
			if(col.r > 0.7 && col.g < 0.5 && col.b < 0.5)
				col = vec4(1,0,0.5,0);		
		}
		if(fantasma == 2){ //azul
			if(col.r > 0.2 && col.g < 0.8 && col.b < 0.8)
				col = vec4(0,0,1,0);		
		}
		if(fantasma == 3){ //naranja bien
			if(col.r > 0.7 && col.g < 0.9 && col.b < 0.9)
				col = vec4(1,0.5,0,0);		
		}
	}
	
    	fragColor = col;
}
