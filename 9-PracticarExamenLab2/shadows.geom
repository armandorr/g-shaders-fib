#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

in vec4 vfrontColor[];
out vec4 gfrontColor;

uniform mat4 modelViewProjectionMatrix;
uniform vec3 boundingBoxMin;

void main( void )
{
	for( int i = 0 ; i < 3 ; i++ )
	{
		gfrontColor = vfrontColor[i];
		gl_Position = modelViewProjectionMatrix*gl_in[i].gl_Position;
		EmitVertex();
	}
    	EndPrimitive();

	gfrontColor = vec4(0);
	for( int i = 0 ; i < 3 ; i++ )
	{
		vec4 V = gl_in[i].gl_Position;
		V.y *= 0.01; 
		V.y += boundingBoxMin.y;
		gl_Position = modelViewProjectionMatrix*V;
		EmitVertex();
	}
    	EndPrimitive();

	for( int i = 0 ; i < 3 ; i++ )
	{
		vec4 V = gl_in[i].gl_Position;
		V.x *= 0.01; 
		V.x += boundingBoxMin.x;
		gl_Position = modelViewProjectionMatrix*V;
		EmitVertex();
	}
    	EndPrimitive();

	for( int i = 0 ; i < 3 ; i++ )
	{
		vec4 V = gl_in[i].gl_Position;
		V.z *= 0.01; 
		V.z += boundingBoxMin.z;
		gl_Position = modelViewProjectionMatrix*V;
		EmitVertex();
	}
    	EndPrimitive();
}
