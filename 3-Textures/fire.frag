#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform sampler2D foc0;
uniform sampler2D foc1;
uniform sampler2D foc2;
uniform sampler2D foc3;

uniform float time;

in vec2 vtexCoord;
uniform float slice = 0.1;

void main()
{
    float num_foc = int(time*10)%4;
    vec4 col;
    if(num_foc == 0) col = texture(foc0, vtexCoord);
    else if(num_foc == 1) col = texture(foc1, vtexCoord);
    else if(num_foc == 2) col = texture(foc2, vtexCoord);
    else col = texture(foc3, vtexCoord);
    fragColor = col;
}
