#version 330 core

uniform sampler2D colorMap;
in vec4 frontColor;
out vec4 fragColor;

in vec2 vtexCoord;
uniform float time;
void main()
{
    int frame = int(floor(time*30))%48;
    int numX = (frame%8);
    int numY = (frame/8);
    float texX = float(numX)/8;
    float texY = 5./6.-(float(numY)/6.);
    vec2 tex = vec2(vtexCoord.x * 1/8.,vtexCoord.y * 1/6. ) + vec2(texX,texY);
    vec4 col = texture(colorMap,tex);
    fragColor = col.a*col;
}
