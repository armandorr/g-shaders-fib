#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform sampler2D map;
in vec2 vtexCoord;

uniform float time;
uniform float a = 0.5;

void main()
{
    vec4 col = texture(map, vtexCoord);
    float m = col.r;
    if(m < col.g) m = col.g;
    if(m < col.b) m = col.b;
    vec2 u = vec2(m);
    float angle = 2*3.14159*time;
    u = mat2(vec2(cos(angle),sin(angle)),vec2(-sin(angle),cos(angle)))*u;
    vec2 tex = vtexCoord+ (a/100.0)*u;
    fragColor = texture(map,tex);
}
