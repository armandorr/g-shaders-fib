#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform sampler2D noise0;
uniform mat4 modelViewMatrix;
uniform mat3 normalMatrix;
in vec2 vtexCoord;
in vec4 v;
in vec3 n;

vec4 shading(vec3 N, vec3 Pos, vec4 diffuse) {
 vec3 lightPos = vec3(0.0,0.0,2.0);
 vec3 L = normalize( lightPos - Pos );
 vec3 V = normalize( -Pos);
 vec3 R = reflect(-L,N);
 float NdotL = max( 0.0, dot( N,L ) );
 float RdotV = max( 0.0, dot( R,V ) );
 float Ispec = pow( RdotV, 20.0 );
 return diffuse * NdotL + Ispec;
}

void main()
{
    vec4 S = 0.3*vec4(0,1,-1,0);
    vec4 T = 0.3*vec4(-2,-1,1,0);
    float s = S.x*v.x + S.y*v.y + S.z*v.z + S.w*v.w;
    float t = T.x*v.x + T.y*v.y + T.z*v.z + T.w*v.w;
    vec4 white = vec4(1.0);
    vec4 redish = vec4(0.5,0.2,0.2,1.0);
    float noise = texture(noise0,vec2(s,t)).r;
    vec4 diff;
    noise *= 2;
    if(noise < 1) diff = mix(white,redish,noise);
    else diff = mix(redish,white,noise-1);
    vec3 N = normalize(normalMatrix * normalize(n));
    vec3 vert = vec3(modelViewMatrix * v);
    fragColor = shading(N,vert,diff);

}
