#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform float time;
uniform sampler2D sampler;
in vec2 vtexCoord;

void main()
{
    vec4 col;
    if(fract(time) <= 0.5){
	col = texture(sampler, vtexCoord);
    }
    else{
        vec2 tex = vtexCoord;
        if(distance(vec2(0.393,0.652), vtexCoord) <= 0.025){
	    tex += vec2(0.057,-0.172);
        }
        col = texture(sampler, tex);
    }
    fragColor = col;
}
