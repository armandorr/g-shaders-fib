#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform sampler2D tex;
in vec2 vtexCoord; 
uniform int textureSize = 1024;
uniform int edgeSize = 2;
uniform float threshold = 0.2; 

void main()
{
    vec2 left = vtexCoord + edgeSize * vec2(-1, 0) / textureSize;
    vec2 right = vtexCoord + edgeSize * vec2(1, 0) / textureSize;
    vec2 bottom = vtexCoord + edgeSize * vec2(0, -1) / textureSize;
    vec2 top = vtexCoord + edgeSize * vec2(0, 1) / textureSize;
    
    vec4 LE = texture(tex,left);
    vec4 RI = texture(tex,right);
    vec4 BO = texture(tex,bottom);
    vec4 TO = texture(tex,top);

    float GX = length(RI - LE);
    float GY = length(TO - BO);
    float L = length(vec2(GX,GY));
    if(L > threshold) fragColor = vec4(0);
    else fragColor = texture(tex,vtexCoord);
}
