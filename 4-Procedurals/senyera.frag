#version 330 core

in vec4 frontColor;
out vec4 fragColor;

in vec2 vtexCoord;

void main()
{
    if(int(fract(vtexCoord.x)*9)%2 == 0) fragColor = vec4(1,1,0,1);
    else fragColor = vec4(1,0,0,1);
}
