#version 330 core

in vec4 frontColor;
out vec4 fragColor;
in vec2 vtexCoord;

uniform bool classic;

void main()
{
    float s = step(0.2,distance(vtexCoord,vec2(0.5)));
    if(classic || s == 0) fragColor = vec4(1,s,s,1);
    else{
	vec2 u = vec2(0.5)-vtexCoord;
	float a = atan(u.y,u.x);
	float s2 = 1-step(mod(a*16/3.14159+0.5,2),1);
	fragColor = vec4(1,s2,s2,1);
    }
}
