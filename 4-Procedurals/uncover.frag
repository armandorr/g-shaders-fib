#version 330 core

in vec4 frontColor;
out vec4 fragColor;
in float X;
uniform float time;

void main()
{
    float coord = (X+1)/2;
    if(coord > time/2.) discard;
    else fragColor = vec4(0,0,1,0);
}
