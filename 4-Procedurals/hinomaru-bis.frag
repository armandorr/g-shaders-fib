#version 330 core

in vec4 frontColor;
out vec4 fragColor;
in vec2 vtexCoord;

void main()
{
    float d = distance(vtexCoord,vec2(0.5));
    float w = length(vec2(dFdx(d), dFdy(d)));
    float s = smoothstep(0.2-w,0.2+w,d);
    fragColor = vec4(1,s,s,1);
}
