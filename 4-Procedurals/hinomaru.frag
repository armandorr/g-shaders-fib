#version 330 core

in vec4 frontColor;
out vec4 fragColor;
in vec2 vtexCoord;

void main()
{
    float s = step(0.2,distance(vtexCoord,vec2(0.5)));
    fragColor = vec4(1,s,s,1);
}
