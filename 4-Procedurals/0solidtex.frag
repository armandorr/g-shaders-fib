#version 330 core

in vec4 frontColor;
out vec4 fragColor;

in vec3 vert;

uniform vec3 origin = vec3(0,0,0);
uniform vec3 axis = vec3(0,0,1);
uniform float slice = 0.05;

void main()
{
    vec3 rect = axis-origin;
    float d = abs(dot(rect,vert));
    float s = d/sqrt(rect.x*rect.x+rect.y*rect.y+rect.z*rect.z);
    int mod = int(s/slice)%2;
    if(mod == 0) fragColor = vec4(0,1,1,0);
    else fragColor = vec4(0,0,1,0);
}
