#version 330 core

in vec4 frontColor;
out vec4 fragColor;

in vec2 vtexCoord;
uniform float n = 8;

void main()
{
    if(int(fract(vtexCoord.x)*n*10.0)%10 == 0) fragColor = vec4(0);
    else if(int(fract(vtexCoord.y)*n*10.0)%10 == 0) fragColor = vec4(0);
    else fragColor = vec4(0.8);
}
