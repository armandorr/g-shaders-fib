#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 color;
layout (location = 3) in vec2 texCoord;

out vec4 frontColor;
out vec2 vtexCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

float M_PI = 3.14159;

vec3 spherize(){
    float theta;
    if(vertex.z > 0) theta = atan(sqrt(vertex.x*vertex.x+vertex.y*vertex.y)/vertex.z);
    else if(vertex.z < 0) theta = M_PI + atan(sqrt(vertex.x*vertex.x+vertex.y*vertex.y)/vertex.z);
    else theta = M_PI/2.0;
    float psi;
    if(vertex.x > 0){
		if(vertex.y >= 0) psi = atan(vertex.y/vertex.x);
		else psi = 2*M_PI + atan(vertex.y/vertex.x);
    }
    else if (vertex.x < 0) psi = M_PI + atan(vertex.y/vertex.x);
    else psi = M_PI/2.0 * sign(vertex.y);

    float spsi = sin(psi); float cpsi = cos(psi);
    float sthe = sin(theta); float cthe = cos(theta);
    vec3 V = vec3(sthe*cpsi,spsi*sthe,cthe);
    return V;
}

void main()
{
    vec3 N = normalize(normalMatrix * normal);
    frontColor = vec4(color,1.0) * N.z;
    vtexCoord = texCoord;
    vec3 V = spherize();
    gl_Position = modelViewProjectionMatrix * vec4(V, 1.0);
}
