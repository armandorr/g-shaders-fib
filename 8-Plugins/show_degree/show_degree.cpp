// GLarena, a plugin based platform to teach OpenGL programming
// © Copyright 2012-2018, ViRVIG Research Group, UPC, https://www.virvig.eu
// 
// This file is part of GLarena
//
// GLarena is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "show_degree.h"
#include "glwidget.h"

void Show_degree::postFrame() 
{
	Scene* s = scene();
	vector< Object > obj = s->objects();
	if(obj.size() == 1){
		QFont font;
		font.setPixelSize(32);
		painter.begin(glwidget());
		painter.setFont(font);

		int numVertexs = (obj[0]).vertices().size();
		vector<int> vectorVertices(numVertexs); //inicialaced to 0
		vector<Face> caras = (obj[0]).faces();
		
		for(int i = 0; i < caras.size(); ++i){
			for(int j = 0; j < caras[i].numVertices(); ++j){
				++vectorVertices[caras[i].vertexIndex(j)];
			}	
		}
		double sumVertices = 0;
		for(int i = 0; i < vectorVertices.size(); ++i){
			sumVertices += vectorVertices[i];
		}
		double mediaVerticesPorCara = sumVertices/numVertexs;
		int x = 15;
		int y = 40;
		QString str = QString::number(mediaVerticesPorCara);
		painter.drawText(x, y, QString(" Degree: " + str));

	  	painter.end();
	}
}
