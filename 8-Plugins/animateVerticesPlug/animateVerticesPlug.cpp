#include "animateVerticesPlug.h"
#include "glwidget.h"

void AnimateVerticesPlug::onPluginLoad()
{
	// Carregar shader, compile & link 
    QString vs_src =
	"#version 330 core\n"
	"in vec3 vertex;"
	"in vec3 normal;"
	"in vec3 color;"
	"out vec4 frontColor;"
	"uniform mat4 modelViewProjectionMatrix;"
	"uniform mat3 normalMatrix;"
	"uniform float time;"
	"void main()"
	"{"
	    "vec3 N = normalize(normalMatrix*normal);"
	    "frontColor = vec4(N.z);"
	    "vec3 V = vertex + normal*vec3(0.1 * sin(2.0*3.14159*1.0*time));"
	    "gl_Position = modelViewProjectionMatrix * vec4(V, 1.0);"
	"}";
    vs = new QOpenGLShader(QOpenGLShader::Vertex, this);
    vs->compileSourceCode(vs_src);
    cout << "VS log:" << vs->log().toStdString() << endl;

    QString fs_src =
      	"#version 330 core\n"
	"in vec4 frontColor;"
	"out vec4 fragColor;"
	"void main()"
	"{"
    	"fragColor = frontColor;"
	"}";
    fs = new QOpenGLShader(QOpenGLShader::Fragment, this);
    fs->compileSourceCode(fs_src);
    cout << "FS log:" << fs->log().toStdString() << endl;

    program = new QOpenGLShaderProgram(this);
    program->addShader(vs);
    program->addShader(fs);
    program->link();
    cout << "Link log:" << program->log().toStdString() << endl;
    	timer.start();

}

void AnimateVerticesPlug::preFrame()
{
	// bind shader and define uniforms
    	program->bind();
    	program->setUniformValue("time", (float)timer.elapsed()/1000);
    	QMatrix4x4 MVP = camera()->projectionMatrix() * camera()->viewMatrix();
    	program->setUniformValue("modelViewProjectionMatrix", MVP);
	QMatrix3x3 NM = (camera()->viewMatrix()).normalMatrix();
   	program->setUniformValue("normalMatrix", NM); 
}

void AnimateVerticesPlug::postFrame()
{
	// unbind shader
    	program->release();
}

void AnimateVerticesPlug::onObjectAdd()
{
	
}

bool AnimateVerticesPlug::drawScene()
{
	return false; // return true only if implemented
}

bool AnimateVerticesPlug::drawObject(int)
{
	return false; // return true only if implemented
}

bool AnimateVerticesPlug::paintGL()
{
	return false; // return true only if implemented
}

void AnimateVerticesPlug::keyPressEvent(QKeyEvent *)
{
	
}

void AnimateVerticesPlug::mouseMoveEvent(QMouseEvent *)
{
	
}

