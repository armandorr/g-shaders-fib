#include "volume.h"
#include "glwidget.h"

void Volume::onPluginLoad()
{
	Scene* scn = scene();
	const vector<Object> obj = scn->objects();
	for(unsigned int i = 0; i < obj.size(); ++i){
		const vector<Vertex> vert = obj[i].vertices();
		const vector<Face> fc = obj[i].faces();
		float sumaTotal = 0;
		for(unsigned int j = 0; j < fc.size(); ++j){
			Point zero = vert[fc[j].vertexIndex(0)].coord();
			Point one  = vert[fc[j].vertexIndex(1)].coord();
			Point two  = vert[fc[j].vertexIndex(2)].coord();
			Point centroide = (zero+one+two)/3.0;
			float Cz = centroide.z();
			float Nz = fc[j].normal().z();
			QVector3D zeroOne = zero-one;
			QVector3D zeroTwo = zero-two;
			QVector3D cross = QVector3D::crossProduct(zeroOne,zeroTwo);
			float A = cross.length()/2.0;
			sumaTotal += Cz*Nz*A;
		}
		cout << "Volume: " << sumaTotal << endl;
	}
}

bool Volume::drawScene()
{
	return false; // return true only if implemented
}

bool Volume::drawObject(int)
{
	return false; // return true only if implemented
}

bool Volume::paintGL()
{
	return false; // return true only if implemented
}

void Volume::keyPressEvent(QKeyEvent *)
{
	
}

void Volume::mouseMoveEvent(QMouseEvent *)
{
	
}

