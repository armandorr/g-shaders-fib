// GLarena, a plugin based platform to teach OpenGL programming
// © Copyright 2012-2018, ViRVIG Research Group, UPC, https://www.virvig.eu
// 
// This file is part of GLarena
//
// GLarena is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "modelInfo2.h"
#include "glwidget.h"

void ModelInfo2::postFrame() 
{
	QFont font;
	font.setPixelSize(32);
	painter.begin(glwidget());
	painter.setFont(font);

	Scene* s = scene();
	int poli = 0;
	vector< Object > obj = s->objects();
	for(int i = 0; i < obj.size(); ++i){
		poli += (obj[i]).faces().size();
	}
	int sum = 0;
	for(int i = 0; i < obj.size(); ++i){
		sum += (obj[i]).vertices().size();
	}
	int triang = 0;
	for(int i = 0; i < obj.size(); ++i){
		vector< Face > fa = (obj[i]).faces();
		for(int j = 0; j < fa.size(); ++j){
			if(fa[j].numVertices() == 3) ++triang;
		}
	}

	int x = 15;
	int y = 40;
	QString str = QString::number(s->objects().size());
	painter.drawText(x, y, QString(" Objectes: " + str));
	
	x = 15;
	y = 480;
	str = QString::number(poli);
	painter.drawText(x, y, QString(" Poligons: " + str));

	x = 480;
	y = 480;
	str = QString::number(sum);
	painter.drawText(x, y, QString(" Vertexs: " + str));

	x = 250;
	y = 40;
	str = QString::number(triang*100.0/poli);
	painter.drawText(x, y, QString(" Percentatge de triangles: " + str + "%"));

  	painter.end();
}
