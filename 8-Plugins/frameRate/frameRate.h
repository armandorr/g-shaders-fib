#ifndef _FRAMERATE_H
#define _FRAMERATE_H

#include "plugin.h" 
#include <QElapsedTimer>

class FrameRate: public QObject, public Plugin
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "Plugin") 
	Q_INTERFACES(Plugin)

  public:
	 void onPluginLoad();
	 void preFrame();
	 void postFrame();

	 void onObjectAdd();
	 bool drawScene();
	 bool drawObject(int);

	 bool paintGL();
  private:
	// add private methods and attributes here
	QOpenGLShaderProgram* program;
    	QOpenGLShader *fs, *vs;
	QElapsedTimer timer;
	QElapsedTimer timer2;
	float frameRate;
};

#endif
