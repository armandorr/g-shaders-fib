#include "frameRate.h"
#include "glwidget.h"

void FrameRate::onPluginLoad()
{
	frameRate = 0;
	timer.start();
    	timer2.start();
}

void FrameRate::preFrame()
{
	// bind shader and define uniforms
	timer.restart();
}

void FrameRate::postFrame()
{
	frameRate += (float)timer.elapsed()/1000;
	if(timer2.elapsed() >= 1000){
		cout << "Framerate: " << frameRate << endl;
		timer2.restart();
		frameRate = 0;
	}
	timer.restart();
}

void FrameRate::onObjectAdd()
{
	
}

bool FrameRate::drawScene()
{
	return false; // return true only if implemented
}

bool FrameRate::drawObject(int)
{
	return false; // return true only if implemented
}

bool FrameRate::paintGL()
{
	return false; // return true only if implemented
}

