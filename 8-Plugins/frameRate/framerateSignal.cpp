#include "framerate.h"
#include "glwidget.h"
#include <QTimer>


void Framerate::clearFPS(){
	cout << "FPS: " << fps << endl;
	fps = 0;
	cout << "CLEAR"<< endl;
}

void Framerate::onPluginLoad()
{
	fps = 0;
	QTimer *frameTimer=new QTimer(this);
	connect(frameTimer, SIGNAL(timeout()), this, SLOT(clearFPS()));
	frameTimer->start(1000);
}

void Framerate::preFrame()
{
	fps += 1;
}

void Framerate::postFrame()
{
	
}

void Framerate::onObjectAdd()
{
	
}

bool Framerate::drawScene()
{
	return false; // return true only if implemented
}

bool Framerate::drawObject(int)
{
	return false; // return true only if implemented
}

bool Framerate::paintGL()
{
	return false; // return true only if implemented
}

void Framerate::keyPressEvent(QKeyEvent *)
{
	
}

void Framerate::mouseMoveEvent(QMouseEvent *)
{
	
}

