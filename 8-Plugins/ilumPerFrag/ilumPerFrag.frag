#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform vec4 lightAmbient; //ia
uniform vec4 lightDiffuse; //id
uniform vec4 lightSpecular; //is
 
uniform vec4 matAmbient; //ka
uniform vec4 matDiffuse; //kd
uniform vec4 matSpecular; //ks
uniform float matShininess; //s

uniform mat4 modelViewMatrix;
uniform mat3 normalMatrix;

in vec3 V;
in vec3 N;

uniform int num = 3;

vec4 phong(vec3 normal, vec3 vertex, vec3 light)
{
	vec3 L = normalize(light.xyz-vertex.xyz);
	float NdotL = max( 0.0, dot( normal,L ) );
	
	vertex = normalize(-vertex.xyz);

	vec3 R = normalize(2.0*dot(normal,L)*normal-L);
	float RdotV = max( 0.0, dot( R,vertex ) );

	float Idiff = NdotL;
	float Ispec = 0;
	if (NdotL>=0) Ispec=pow( RdotV, matShininess );
	
	return matAmbient*lightAmbient + matDiffuse*lightDiffuse*Idiff + matSpecular*lightSpecular*Ispec;
}

void main()
{
    	vec3 norm, vert, L;
    	vec4 light = vec4(1.0,1.0,1.0,0);

	norm = normalize(normalMatrix*N);
	vert = (modelViewMatrix*vec4(V,1.0)).xyz;
	L = light.xyz;

    	fragColor = phong(norm,vert,L);
}
