#include "ilumPerFrag.h"
#include "glwidget.h"

void IlumPerFrag::onPluginLoad()
{
	// Carregar shader, compile & link 
    vs = new QOpenGLShader(QOpenGLShader::Vertex, this);
    vs->compileSourceFile("ilumPerFrag.vert");
    cout << "VS log:" << vs->log().toStdString() << endl;

    fs = new QOpenGLShader(QOpenGLShader::Fragment, this);
    fs->compileSourceFile("ilumPerFrag.frag");
    cout << "FS log:" << fs->log().toStdString() << endl;

    program = new QOpenGLShaderProgram(this);
    program->addShader(vs);
    program->addShader(fs);
    program->link();
    cout << "Link log:" << program->log().toStdString() << endl;
}

void IlumPerFrag::preFrame()
{
	// bind shader and define uniforms
    	program->bind();
    	QMatrix4x4 MVP = camera()->projectionMatrix() * camera()->viewMatrix();
    	program->setUniformValue("modelViewProjectionMatrix", MVP);
	QMatrix4x4 MV = camera()->viewMatrix();
    	program->setUniformValue("modelViewMatrix", MV);
	QMatrix3x3 NM = (camera()->viewMatrix()).normalMatrix();
   	program->setUniformValue("normalMatrix", NM);
	QVector4D MA = QVector4D(0.1,0.1,0.1,1);
   	program->setUniformValue("matAmbient", MA); 
	QVector4D MD = QVector4D(0.9,0.5,0.1,1);
   	program->setUniformValue("matDiffuse", MD); 
	QVector4D MS = QVector4D(1.0,1.0,1.0,1);
   	program->setUniformValue("matSpecular", MS); 
	float shin = 150.0;
   	program->setUniformValue("matShininess", shin); 
	QVector4D LA = QVector4D(0.3,0.3,0.3,1);
   	program->setUniformValue("lightAmbient", LA);
	QVector4D LD = QVector4D(0.6,0.6,0.6,1);
   	program->setUniformValue("lightDiffuse", LD);
	QVector4D LS = QVector4D(0.5,0.6,0.7,1);
   	program->setUniformValue("lightSpecular", LS);
}

void IlumPerFrag::postFrame()
{
	// unbind shader
    	program->release();
}

void IlumPerFrag::onObjectAdd()
{
	
}

bool IlumPerFrag::drawScene()
{
	return false; // return true only if implemented
}

bool IlumPerFrag::drawObject(int)
{
	return false; // return true only if implemented
}

bool IlumPerFrag::paintGL()
{
	return false; // return true only if implemented
}

void IlumPerFrag::keyPressEvent(QKeyEvent *)
{
	
}

void IlumPerFrag::mouseMoveEvent(QMouseEvent *)
{
	
}
