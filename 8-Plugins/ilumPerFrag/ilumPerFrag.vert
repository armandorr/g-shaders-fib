#version 330 core

in vec3 vertex;
in vec3 normal;
in vec3 color;

out vec4 frontColor;
out vec2 vtexCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

out vec3 N;
out vec3 V;

void main()
{
    N = normal;
    V = vertex;
    vec3 norm = normalize(normalMatrix * normal);
    frontColor = vec4(color,1.0) * norm.z;
    gl_Position = modelViewProjectionMatrix * vec4(vertex, 1.0);
}
