#include "modelInfo1.h"
#include "glwidget.h"

void ModelInfo1::info(){
	Scene* s = scene();
	cout << "Objectes: " << s->objects().size() << endl;
	cout << "Poligons: ";
	int poli = 0;
	vector< Object > obj = s->objects();
	for(int i = 0; i < obj.size(); ++i){
		poli += (obj[i]).faces().size();
	}
	cout << poli << endl;
	cout << "Vertexs: ";
	int sum = 0;
	for(int i = 0; i < obj.size(); ++i){
		sum += (obj[i]).vertices().size();
	}
	cout << sum << endl;
	cout << "Triangulos: ";
	int triang = 0;
	for(int i = 0; i < obj.size(); ++i){
		vector< Face > fa = (obj[i]).faces();
		for(int j = 0; j < fa.size(); ++j){
			if(fa[j].numVertices() == 3) ++triang;
		}
	}
	cout << triang*100.0/poli << "%" << endl;
}

void ModelInfo1::onPluginLoad()
{
	info();
}

void ModelInfo1::preFrame()
{
	
}

void ModelInfo1::postFrame()
{
		
}

void ModelInfo1::onObjectAdd()
{
	info();
}

bool ModelInfo1::drawScene()
{
	return false; // return true only if implemented
}

bool ModelInfo1::drawObject(int)
{
	return false; // return true only if implemented
}

bool ModelInfo1::paintGL()
{
	return false; // return true only if implemented
}

void ModelInfo1::keyPressEvent(QKeyEvent *)
{
	
}

void ModelInfo1::mouseMoveEvent(QMouseEvent *)
{
	
}

