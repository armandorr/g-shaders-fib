#version 330 core

in vec4 frontColor;
out vec4 fragColor;

in vec2 vtexCoord;
uniform sampler2D colorMap;

uniform float time;

void main()
{
	int unitats = int(time)%10;
	int desenes = (int(time)/10)%10;
	int centenes = (int(time)/100)%10;
	vec2 digit;
    	if(vtexCoord.s < 1) digit = vtexCoord*vec2(1./10., 1) + vec2(centenes/10.,0);
   	else if(vtexCoord.s < 2) digit = (vtexCoord-vec2(1, 0)) * vec2(1./10., 1) + vec2(desenes/10.,0);
  	else digit = (vtexCoord-vec2(2, 0)) * vec2(1./10., 1) + vec2(unitats/10.,0);
  	vec4 col = texture(colorMap, digit);
  	if(col.a < 0.5) discard;
  	else fragColor = vec4(1,0,0,0);
}
