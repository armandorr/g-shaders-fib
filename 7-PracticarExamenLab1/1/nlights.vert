#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat3 normalMatrix;

out vec4 vert;
out vec3 norm;

void main()
{
    vec3 N = normalize(normalMatrix * normal);
    norm = N;
    vert = modelViewMatrix*vec4(vertex,1.0);
    gl_Position = modelViewProjectionMatrix * vec4(vertex, 1.0);
}
