#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 color;
layout (location = 3) in vec2 texCoord;

out vec4 frontColor;
out vec2 vtexCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

void main()
{
    	vec3 N = normalize(normalMatrix * normal);
    	frontColor = vec4(color,1.0);
	float maxValue = abs(vertex.x);
	if(maxValue < abs(vertex.y)) maxValue = abs(vertex.y);
	if(maxValue < abs(vertex.z)) maxValue = abs(vertex.z);
	vec3 V = vertex/maxValue;
    	gl_Position = modelViewProjectionMatrix * vec4(V, 1.0);
}
