#version 330 core

out vec4 fragColor;

uniform vec4 lightAmbient; //ia
uniform vec4 lightDiffuse; //id
uniform vec4 lightSpecular; //is
 
uniform vec4 matAmbient; //ka
uniform vec4 matDiffuse; //kd
uniform vec4 matSpecular; //ks
uniform float matShininess; //s

in vec4 vert;
in vec3 norm;

uniform int n = 10;
const float pi = 3.141592;

vec4 phong(vec3 lightPosition){
    vec3 L = normalize(lightPosition.xyz-vert.xyz);
    //vec4 col1 = matAmbient*lightAmbient;
    vec4 col2 = matDiffuse*lightDiffuse * max(0.0, dot(norm, L))/sqrt(n);

    if(dot(norm,L) < 0) return col2;

    vec3 R = normalize(2 * dot(norm,L)*norm -L);
    vec3 V = normalize(-vert.xyz);

    vec4 col3 = matSpecular*lightSpecular * pow(max(0.0, dot(R, V)),matShininess);

    return col2+col3;
}



void main()
{
	vec3 light = vec3(10,0,0);
	vec4 color = vec4(0);
	float angle = 2*pi / n;
	float cs = cos(angle);
	float sn = sin(angle);
	mat3 R = mat3(vec3(cs, sn, 0), vec3(-sn, cs,0), vec3(0,0,1));
	for(int i = 0; i < n; ++i){
		color += phong(light);
		light = R*light;
	}
	fragColor = color;
}
