#version 330 core

in vec4 frontColor;
out vec4 fragColor;
in vec2 vtexCoord;
uniform sampler2D fbm;
uniform float time;
const float pi = 3.14159;

vec4 red = vec4(1,0,0,0);
vec4 groc = vec4(1,1,0,0);
vec4 verd = vec4(0,1,0,0);
vec4 cian = vec4(0,1,1,0);
vec4 blau = vec4(0,0,1,0);
vec4 mag = vec4(1,0,1,0);

vec4 colors[7];

void main()
{

	colors[0] = red;
	colors[1] = groc;
	colors[2] = verd;
	colors[3] = cian;
	colors[4] = blau;
	colors[5] = mag;
	colors[6] = red;
	float r = texture(fbm,vtexCoord).r;
	float s = 3*(sin(2*pi*0.1*time + 2*pi*r)+1);
	fragColor=mix(colors[int(s)], colors[int(s)+1],fract(s));

	/*
	float r = texture(fbm,vtexCoord).r;
	float s = 1*sin(2*pi*0.1*time + 2*pi*r); //0..2
	vec4 col;
	if(s <= -2./3.) col = mix(red,groc,(s+1)*3.);
	else if(s <= -1./3.) col = mix(groc,verd,(s+2./3.)*3.);
	else if(s <= 0) col = mix(verd,cian,(s+1./3.)*3.);
	else if(s <= 1/3.) col = mix(cian,blau,s*3.);
	else if(s <= 2/3.) col = mix(blau,mag,(s-1./3.)*3.);
	else col = mix(mag,red,(s-2./3.)*3.);
	fragColor = col;
	*/
}
