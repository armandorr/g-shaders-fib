#version 330 core

in vec4 frontColor;
out vec4 fragColor;
in vec3 N;
in vec2 vtexCoord;
uniform sampler2D colorMap;

void main()
{
	vec2 C1 = vec2(0.34,0.65)-0.1*N.xy;
	vec2 C2 = vec2(0.66,0.65)-0.1*N.xy;
	float d1 = distance(vtexCoord,C1);
	float d2 = distance(vtexCoord,C2);
	if(step(d1,0.05)!=step(d2,0.05)) fragColor = vec4(0);
    	else fragColor = texture(colorMap,vtexCoord);
}
