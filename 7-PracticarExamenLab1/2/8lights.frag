#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform vec4 lightAmbient; //ia
uniform vec4 lightDiffuse; //id
uniform vec4 lightSpecular; //is
uniform vec4 lightPosition;
 
uniform vec4 matAmbient; //ka
uniform vec4 matDiffuse; //kd
uniform vec4 matSpecular; //ks
uniform float matShininess; //s

uniform mat4 modelMatrix;
uniform mat4 modelMatrixInverse;
uniform mat4 viewMatrix;
uniform mat4 viewMatrixInverse;
uniform mat4 modelViewMatrix;
uniform mat4 modelViewMatrixInverse;
uniform mat3 normalMatrix;
uniform mat3 normalMatrixInverse;

in vec3 V;
in vec3 N;

uniform int n = 2;
const float pi = 3.141592;
uniform vec3 boundingBoxMin;
uniform vec3 boundingBoxMax;

vec4 phong(vec3 normal, vec3 vertex, vec3 light)
{
	vec3 L = normalize(light.xyz-vertex.xyz);
	float NdotL = max( 0.0, dot( normal,L ) );
	
	vertex = normalize(viewMatrixInverse[3].xyz-vertex); 

	vec3 R = normalize(2.0*dot(normal,L)*normal-L);
	float RdotV = max( 0.0, dot( R,vertex ) );

	float Idiff = NdotL/2.0;
	float Ispec = 0;
	if (NdotL>=0) Ispec=pow( RdotV, matShininess );
	
	return matDiffuse*lightDiffuse*Idiff + matSpecular*lightSpecular*Ispec;
}

void main()
{
    	vec3 norm, vert, L;

        norm = normalize(inverse(transpose(mat3(modelMatrix)))*N);
	vert = (modelMatrix*vec4(V,1.0)).xyz;
        
	vec3 min = boundingBoxMin;
	vec3 max = boundingBoxMax;

	L = vec3(min.x,min.y,min.z);
    	vec4 col = phong(norm, vert, L);
	L = vec3(min.x,min.y,max.z);
    	col += phong(norm, vert, L);
	L = vec3(min.x,max.y,min.z);
    	col += phong(norm, vert, L);
	L = vec3(min.x,max.y,max.z);
    	col += phong(norm, vert, L);
	L = vec3(max.x,min.y,min.z);
    	col += phong(norm, vert, L);
	L = vec3(max.x,min.y,max.z);
    	col += phong(norm, vert, L);
	L = vec3(max.x,max.y,min.z);
    	col += phong(norm, vert, L);
	L = vec3(max.x,max.y,max.z);
    	col += phong(norm, vert, L);

	fragColor = col;
}
