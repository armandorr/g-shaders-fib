#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform vec4 lightAmbient; //ia
uniform vec4 lightDiffuse; //id
uniform vec4 lightSpecular; //is
uniform vec4 lightPosition;
 
uniform vec4 matAmbient; //ka
uniform vec4 matDiffuse; //kd
uniform vec4 matSpecular; //ks
uniform float matShininess; //s

uniform mat4 modelMatrix;
uniform mat4 modelMatrixInverse;
uniform mat4 viewMatrix;
uniform mat4 viewMatrixInverse;
uniform mat4 modelViewMatrix;
uniform mat4 modelViewMatrixInverse;
uniform mat3 normalMatrix;
uniform mat3 normalMatrixInverse;

in vec3 V;
in vec3 N;

uniform int num = 3;

vec4 phong(vec3 normal, vec3 vertex, vec3 light)
{
	vec3 L = normalize(light.xyz-vertex.xyz);
	float NdotL = max( 0.0, dot( normal,L ) );
	
	//object
	if(num == 1) vertex = normalize(modelViewMatrixInverse[3].xyz-vertex);
	//word
	if(num == 2) vertex = normalize(viewMatrixInverse[3].xyz-vertex); 
	//eye
	if(num == 3) vertex = normalize(-vertex.xyz);

	vec3 R = normalize(2.0*dot(normal,L)*normal-L);
	float RdotV = max( 0.0, dot( R,vertex ) );

	float Idiff = NdotL;
	float Ispec = 0;
	if (NdotL>=0) Ispec=pow( RdotV, matShininess );
	
	return matAmbient*lightAmbient + matDiffuse*lightDiffuse*Idiff + matSpecular*lightSpecular*Ispec;
}

void main()
{
    vec3 norm, vert, L;
    vec4 light = lightPosition;

    if(num == 1){ //object
        norm = normalize(N);
	vert = V;
        L = (modelViewMatrixInverse*light).xyz;
    }

    else if(num == 2){ //world
        norm = normalize(inverse(transpose(mat3(modelMatrix)))*N);
	vert = (modelMatrix*vec4(V,1.0)).xyz;
        L = (viewMatrixInverse*light).xyz;
    }

    else if(num == 3){ //eye
        norm = normalize(normalMatrix*N);
	vert = (modelViewMatrix*vec4(V,1.0)).xyz;
	L = light.xyz;
    }

    fragColor = phong(norm,vert,L);
}
