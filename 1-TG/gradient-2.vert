#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 color;
layout (location = 3) in vec2 texCoord;

out vec4 frontColor;
out vec2 vtexCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

void main()
{
    vec3 N = normalize(normalMatrix * normal);
    
    vtexCoord = texCoord;
    vec4 Clip = modelViewProjectionMatrix * vec4(vertex, 1.0);
    vec3 NDC = vec3(Clip.x/Clip.w,Clip.y/Clip.w,Clip.z/Clip.w);
    vec3 M;
    if(NDC.y <= -1.0) M = vec3(1,0,0);
    else if(NDC.y <= -0.5) M = mix(vec3(1,0,0),vec3(1,1,0),(NDC.y+1.0)/0.5);
    else if(NDC.y <= 0.0) M = mix(vec3(1,1,0),vec3(0,1,0),(NDC.y+0.5)/0.5);
    else if(NDC.y <= 0.5) M = mix(vec3(0,1,0),vec3(0,1,1),(NDC.y)/0.5);
    else if(NDC.y <= 1.0) M = mix(vec3(0,1,1),vec3(0,0,1),(NDC.y-0.5)/0.5);
    else M = vec3(0,0,1);

    frontColor = vec4(M,1.0);
    gl_Position = Clip;
}
