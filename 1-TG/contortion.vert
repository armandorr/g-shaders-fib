#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 color;
layout (location = 3) in vec2 texCoord;

out vec4 frontColor;
out vec2 vtexCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

uniform float time;

void main()
{
    vec3 N = normalize(normalMatrix * normal);
    frontColor = vec4(color,1.0);
    vtexCoord = texCoord;
    float angle;
    if(vertex.y < 0.5) angle = 0.0;
    else angle = (vertex.y-0.5)*sin(time);
    vec4 V = mat4(vec4(1,0,0,0),vec4(0,1,0,0),vec4(0,0,1,0),vec4(0,-1,0,1))*vec4(vertex,1.0);
    vec3 gir = mat3(vec3(1,0,0),vec3(0,cos(angle),sin(angle)),vec3(0,-sin(angle),cos(angle)))*vec3(V);
    vec4 VE = mat4(vec4(1,0,0,0),vec4(0,1,0,0),vec4(0,0,1,0),vec4(0,1,0,1))*vec4(gir,1.0);
    gl_Position = modelViewProjectionMatrix * VE;
}
