#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 color;
layout (location = 3) in vec2 texCoord;

out vec4 frontColor;
out vec2 vtexCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;
uniform vec3 boundingBoxMin;
uniform vec3 boundingBoxMax;

void main()
{
    vec3 N = normalize(normalMatrix * normal);
    float grad = (vertex.y-boundingBoxMin.y)/(boundingBoxMax.y-boundingBoxMin.y);
    vec3 M = color;

    if(grad <= 0.25) M = mix(vec3(1,0,0),vec3(1,1,0),grad/0.25);
    else if(grad <= 0.5) M = mix(vec3(1,1,0),vec3(0,1,0),(grad-0.25)/0.25);
    else if(grad <= 0.75) M = mix(vec3(0,1,0),vec3(0,1,1),(grad-0.50)/0.25);
    else M = mix(vec3(0,1,1),vec3(0,0,1),(grad-0.75)/0.25);

    frontColor = vec4(M,1.0);
    vtexCoord = texCoord;
    gl_Position = modelViewProjectionMatrix * vec4(vertex, 1.0);
}
