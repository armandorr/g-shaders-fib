#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 color;
layout (location = 3) in vec2 texCoord;

out vec4 frontColor;
out vec2 vtexCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

uniform mat4 modelViewMatrixInverse;
uniform vec4 lightPosition;
uniform float n = 4;

void main()
{
    vec3 N = normalize(normalMatrix * normal);
    frontColor = vec4(N.z);
    vtexCoord = texCoord;
    vec4 F = modelViewMatrixInverse*lightPosition;

    float w = clamp(1/pow(distance(vertex,vec3(F)),n),0,1);
    vec3 V = (1.0-w)*vertex + w*vec3(F);
    gl_Position = modelViewProjectionMatrix * vec4(V,1.0);
}
