#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

in vec4 vfrontColor[];

out vec4 gfrontColor;

uniform vec3 boundingBoxMin, boundingBoxMax;
uniform mat4 modelViewProjectionMatrix;
uniform mat4 viewMatrixInverse;

void emitVertex(vec3 V){
	gl_Position = modelViewProjectionMatrix*vec4(V,1);
	EmitVertex();
}

void emitStrip4(vec3 V0, vec3 V1, vec3 V2, vec3 V3, vec3 color){
	gfrontColor = vec4(color,1);
	emitVertex(V0);
	emitVertex(V1);
	emitVertex(V2);
	emitVertex(V3);
	EndPrimitive();
}

bool inside(vec3 V){
	bool x = V.x > boundingBoxMin.x && V.x < boundingBoxMax.x;
	bool y = V.y > boundingBoxMin.y && V.y < boundingBoxMax.y;
	bool z = V.z > boundingBoxMin.z && V.z < boundingBoxMax.z;
	return x && y && z;
}

void main( void )
{
	for( int i = 0 ; i < 3 ; i++ )
	{
		if(inside(viewMatrixInverse[3].xyz)) gfrontColor = 2*vfrontColor[i];
		else gfrontColor = vfrontColor[i];
		gl_Position = modelViewProjectionMatrix*gl_in[i].gl_Position;
		EmitVertex();
	}
    	EndPrimitive();
	if(gl_PrimitiveIDIn == 0){
		vec3 V0 = vec3(boundingBoxMin.x,boundingBoxMin.y,boundingBoxMin.z);
		vec3 V1 = vec3(boundingBoxMax.x,boundingBoxMin.y,boundingBoxMin.z);
		vec3 V2 = vec3(boundingBoxMin.x,boundingBoxMax.y,boundingBoxMin.z);
		vec3 V3 = vec3(boundingBoxMax.x,boundingBoxMax.y,boundingBoxMin.z);
		vec3 V4 = vec3(boundingBoxMin.x,boundingBoxMin.y,boundingBoxMax.z);
		vec3 V5 = vec3(boundingBoxMax.x,boundingBoxMin.y,boundingBoxMax.z);
		vec3 V6 = vec3(boundingBoxMin.x,boundingBoxMax.y,boundingBoxMax.z);
		vec3 V7 = vec3(boundingBoxMax.x,boundingBoxMax.y,boundingBoxMax.z);
		emitStrip4(V4,V0,V6,V2,vec3(1,0,0)); //esquerra
		emitStrip4(V4,V5,V0,V1,vec3(0,1,0)); //abaix
		emitStrip4(V1,V5,V3,V7,vec3(1,0,0)); //dreta
		emitStrip4(V0,V1,V2,V3,vec3(0,0,1)); //darrere
	}
}
