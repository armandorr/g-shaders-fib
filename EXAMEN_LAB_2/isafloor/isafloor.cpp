#include "isafloor.h"
#include "glwidget.h"

void Isafloor::onPluginLoad()
{
	vs = new QOpenGLShader(QOpenGLShader::Vertex, this);
	vs->compileSourceFile("isafloor.vert");
	cout << "VS log:" << vs->log().toStdString() << endl;

	fs = new QOpenGLShader(QOpenGLShader::Fragment, this);
	fs->compileSourceFile("isafloor.frag");
	cout << "FS log:" << fs->log().toStdString() << endl;

	program = new QOpenGLShaderProgram(this);
	program->addShader(vs);
	program->addShader(fs);
	program->link();
	cout << "Link log:" << program->log().toStdString() << endl;


	Scene* scn = scene();
	const vector< Object > obj = scn->objects();
	const vector< Face > faces = obj[0].faces();
	const vector<Vertex> vert = obj[0].vertices();
	float prop = 0;
	float total = 0;	
	QVector3D V = QVector3D(0,0,1);
	for(int i = 0; i < faces.size(); ++i){
		Point zero = vert[faces[i].vertexIndex(0)].coord();
		Point one  = vert[faces[i].vertexIndex(1)].coord();
		Point two  = vert[faces[i].vertexIndex(2)].coord();
		QVector3D norm = (faces[i].normal()).normalized();
		QVector3D zeroOne = zero-one;
		QVector3D zeroTwo = zero-two;
		QVector3D cross = QVector3D::crossProduct(zeroOne,zeroTwo);
		float superficie = cross.length()/2.0;
		total += superficie;
		if(QVector3D::dotProduct(norm, V) > 0.7){
			prop += superficie;
		}
	}
	cout << "TERRA: " << prop/total << endl;
	proporcio = prop/total;
}

void Isafloor::preFrame()
{
	program->bind();
    	QMatrix4x4 MVP = camera()->projectionMatrix() * camera()->viewMatrix();
    	program->setUniformValue("modelViewProjectionMatrix", MVP);
	QMatrix3x3 NM = (camera()->viewMatrix()).normalMatrix();
	program->setUniformValue("normalMatrix", NM);
	program->setUniformValue("prop", proporcio);
}

void Isafloor::postFrame()
{
	program->release();
}

void Isafloor::onObjectAdd()
{

}

bool Isafloor::drawScene()
{
	return false; // return true only if implemented
}

bool Isafloor::drawObject(int)
{
	return false; // return true only if implemented
}

bool Isafloor::paintGL()
{
	return false; // return true only if implemented
}


