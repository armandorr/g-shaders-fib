#version 330 core

in vec4 frontColor;
out vec4 fragColor;
in vec2 vtexCoord;
uniform sampler2D colorMap;

bool boundings(vec2 vtex, vec2 boundX, vec2 boundY){
	bool x = vtex.x > boundX.x && vtex.x < boundX.y;
	bool y = vtex.y > boundY.x && vtex.y < boundY.y;
	return x && y;
}

vec2 getBounds(vec2 vtex){
	bool found = false;
	float x;
	for(float i = 1/6.; !found && i <= 2; i+=1/6.){
		if(vtex.x < i){
			x = i;
			found = true;
		}
	}
	float boundX = x-1/6.;

	float y;
	found = false;
	for(float j = 1; !found && j <= 12; ++j){
		if(vtex.y < j){
			y = j;
			found = true;
		}
	}
	float boundY = y-1.0;
	return vec2(boundX,boundY);
}

void main()
{
	vec4 col = vec4(0);
	if(boundings(vtexCoord,vec2(1/6.,3/6.),vec2(1,2))) //Pacman
		col = texture(colorMap,vtexCoord);
	else if(boundings(vtexCoord,vec2(3/6.,4/6.),vec2(5,6))) //Fantasma
		col = texture(colorMap,vtexCoord+vec2(3/6.,0));
	else if(boundings(vtexCoord,vec2(8/6.,9/6.),vec2(3,4))) //Fantasma
		col = texture(colorMap,vtexCoord+vec2(-2/6.,0));
	else{
		vec2 bounds = getBounds(vtexCoord);
		for(float i = 1; i <= 12; ++i){
			if(boundings(vtexCoord,vec2(11/6.,2),vec2(i,i+1)))
				col = texture(colorMap,vtexCoord+vec2(4/6.,0));
			if(boundings(vtexCoord,vec2(0,1/6.),vec2(i,i+1)))
				col = texture(colorMap,vtexCoord+vec2(3/6.,0));
		}

	}
    	fragColor = col;
}
