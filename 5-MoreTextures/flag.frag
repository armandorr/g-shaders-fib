#version 330 core
in vec4 frontColor;
out vec4 fragColor;

in vec2 vtexCoord;

void main()
{
    float dist = distance(vec2(vtexCoord.s,vtexCoord.t*3./4.),vec2(0.25,0.75/2.));
    float dist2 = distance(vec2(vtexCoord.s,vtexCoord.t*3./4.),vec2(0.38,0.75/2.));
    float distSquare = distance(vtexCoord.s,0.75);
    float distSquare2 = distance(vtexCoord.t,0.5);

    if (step(0.2,dist) == 0.0 && step(0.20,dist2) == 1.0) discard;
    else if (step(0.15*3./4.,distSquare) == 0.0 && step(0.15,distSquare2) == 0.0) discard;
    else fragColor = vec4(0.2,0.5,0.2,1);
    
}
