#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform sampler2D colorMap;
in vec2 vtexCoord;

void main()
{
    vec2 digit;
    if(vtexCoord.s < 1./6.) digit = vtexCoord*vec2(6./10., 1) + vec2(0.4,0);
    else if(vtexCoord.s < 2./6.) digit = (vtexCoord-vec2(1./6, 0)) * vec2(6./10., 1) + vec2(0.8,0);
    else if(vtexCoord.s < 3./6.) digit = (vtexCoord-vec2(2./6, 0)) * vec2(6./10., 1) + vec2(0.1,0);
    else if(vtexCoord.s < 4./6.) digit = (vtexCoord-vec2(3./6, 0)) * vec2(6./10., 1) + vec2(0.7,0);
    else if(vtexCoord.s < 5./6.) digit = (vtexCoord-vec2(4./6, 0)) * vec2(6./10., 1) + vec2(0.1,0);
    else digit = (vtexCoord-vec2(5./6, 0)) * vec2(6./10., 1) + vec2(0.2,0);
    vec4 col = texture(colorMap, digit);
    if(col.a < 0.5) discard;
    else fragColor = vec4(0,0,1,0);
}
