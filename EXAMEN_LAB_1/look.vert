#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;

out vec4 frontColor;

uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

uniform vec2 mousePosition;
uniform float mouseOverrideX = -1;
uniform vec2 viewport = vec2(800,600);

void main()
{
	float X;
	if(mouseOverrideX < 0) X = mousePosition.x;
	else X = mouseOverrideX; 

	float alpha = 2.* X/(viewport.x) - 1.0;
	float sina = sin(alpha);
	float cosa = cos(alpha);
	mat3 R = mat3(vec3(cosa,0,-sina),vec3(0,1,0),vec3(sina,0,cosa));
	vec3 Prot = R*vertex;
	vec3 Nrot = R*normal;
	float t = smoothstep(1.45,1.55,vertex.y);
	vec3 P = mix(vertex,Prot,t);
	vec3 N = mix(normal,Nrot,t);
	N = normalize(normalMatrix * N);
    	frontColor = vec4(N.z) ;
    	gl_Position = modelViewProjectionMatrix * vec4(P, 1.0);
}
