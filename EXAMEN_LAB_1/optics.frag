#version 330 core

out vec4 fragColor;
in vec3 V;
uniform sampler2D tex;
uniform float time = 0;
in vec2 vtexCoord;
const float eta = 1.7;
uniform mat4 modelViewMatrixInverse;

/* 
 * Traçat d'un raig a través de l'esfera unitat
 * Entrada: V, un punt sobre l'esfera unitat centrada a l'origen
 * Sortida: P, el punt de sortida d'un raig incident a V provenint de la càmera
 *          dir, vector unitari corresponent a la direcció de sortida del raig
 * Globals: modelViewMatrixInverse, per extreure'n la posició de la càmera en object space
 *          eta, raó dels coeficients de refracció.
 */

void trace(vec3 V, out vec3 P, out vec3 dir) {
    vec3 w = normalize(V-modelViewMatrixInverse[3].xyz);
    vec3 ref = refract(w, normalize(V), 1/eta);
    P = V-2*dot(V,ref)*ref;
    dir = refract(ref, -normalize(P),eta);
}

void main()
{
	if(V.z < -1.5) fragColor = texture(tex,vtexCoord);
	else{
		vec3 P, dir;
		trace(V,P,dir);
		float d = distance(P,vec3(P.x,P.y,-1.5-pow(sin(time),2)));
		vec3 punt = P-2*dir*d;
		if(punt.x < 2.0 || punt.x > -2.0 || punt.y < 2.0 || punt.y > -2.0) fragColor = vec4(0.97);
    		else fragColor = texture(tex,vtexCoord);
	}
}
