#version 330 core

in vec4 frontColor;
out vec4 fragColor;

in vec3 V;
in vec3 N;
in vec2 vtexCoord;
uniform sampler2D colorMap;

uniform vec4 lightAmbient; //ia
uniform vec4 lightDiffuse; //id
uniform vec4 lightSpecular; //is
uniform vec4 lightPosition;
 
uniform vec4 matAmbient; //ka
uniform vec4 matDiffuse; //kd
uniform vec4 matSpecular; //ks
uniform float matShininess; //s

uniform mat4 modelMatrix;
uniform mat4 modelMatrixInverse;
uniform mat4 viewMatrix;
uniform mat4 viewMatrixInverse;
uniform mat4 modelViewMatrix;
uniform mat4 modelViewMatrixInverse;
uniform mat3 normalMatrix;
uniform mat3 normalMatrixInverse;

uniform float time;

vec4 phong(vec3 normal, vec3 vertex, vec3 light)
{
	vec3 L = normalize(light.xyz-vertex.xyz);
	float NdotL = max( 0.0, dot( normal,L ) );
	
	vertex = normalize(-vertex.xyz);

	vec3 R = normalize(2.0*dot(normal,L)*normal-L);
	float RdotV = max( 0.0, dot( R,vertex ) );

	float Idiff = NdotL;
	float Ispec = 0;
	if (NdotL>=0) Ispec=pow( RdotV, matShininess );

	vec3 negre = vec3(0);
	vec3 blanc = vec3(0.8);
	vec4 lightDiffuse;
	if(int(time)%2 != 0) lightDiffuse = vec4(mix(blanc,negre,fract(time)),0.0);
	else lightDiffuse = vec4(mix(negre,blanc,fract(time)),0.0);
	
	int frame = int((time)/2.0) % 12;
	int x = frame/3;
	int y = frame%3; 
	float texX = float(x)/4.;
	float texY = 2./3. - float(y)/3.;
	vec2 tex = vec2(vtexCoord.x*1/4., vtexCoord.y * 1/3.) + vec2(texX,texY);
	vec4 matDiffuse = texture(colorMap,tex);

	return matDiffuse*lightDiffuse*Idiff + matSpecular*lightSpecular*Ispec;
}

void main()
{
    fragColor = phong(N,V,lightPosition.xyz);
}
