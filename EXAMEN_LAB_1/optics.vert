#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 3) in vec2 texCoord;

out vec4 frontColor;
out vec2 vtexCoord;
out vec3 V;
uniform mat4 modelViewProjectionMatrix;
uniform mat3 normalMatrix;

uniform float time = 0;

void main()
{
	vec3 vert = vertex;
	if(vertex.z == -1.5) vert.z = -1.5 - pow(sin(time),2);
	V = vert;
    	vec3 N = normalize(normalMatrix * normal);
    	vtexCoord = texCoord;
    	gl_Position = modelViewProjectionMatrix * vec4(vert, 1.0);
}
