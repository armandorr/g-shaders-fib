#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

in vec4 vfrontColor[];
in vec3 vnormal[];

out vec3 gnormal;

uniform mat4 modelViewProjectionMatrix;
uniform float d = 0.4;

vec3 getNormal(vec3 v1, vec3 v2, vec3 v3) { //ma dreta
  	return normalize(cross(v2-v1, v3-v1))
}

void emitVertex(vec3 V){
	gl_Position = modelViewProjectionMatrix * vec4(V,1);
	EmitVertex();
}

void emitStrip3(vec3 V0, vec3 V1, vec3 V2, vec3 normal){
	gnormal = normal;
	emitVertex(V0);
	emitVertex(V1);
	emitVertex(V2);
	EndPrimitive();
}

void emitStrip4(vec3 V0, vec3 V1, vec3 V2, vec3 V3, vec3 normal){
	gnormal = normal;
	emitVertex(V0);
	emitVertex(V1);
	emitVertex(V2);
	emitVertex(V3);
	EndPrimitive();
}

void main( void )
{
	vec3 N  = normalize((vnormal[0]+vnormal[1]+vnormal[2])/3.0);
	vec3 V0 = (gl_in[0].gl_Position).xyz;
	vec3 V1 = (gl_in[1].gl_Position).xyz;
	vec3 V2 = (gl_in[2].gl_Position).xyz;
	vec3 A0 = V0+d*N;
	vec3 A1 = V1+d*N;
	vec3 A2 = V2+d*N;
	
	emitStrip3(V0,V2,V1,-N);

	emitStrip3(A0,A1,A2, N);

	vec3 normalCalculada = getNormal(V2,V0,A2);
	emitStrip4(V2,V0,A2,A0,normalCalculada);

	normalCalculada = getNormal(V0,V1,A0); //right
	emitStrip4(V0,V1,A0,A1,normalCalculada);

	normalCalculada = getNormal(V1,V2,A1);
	emitStrip4(V1,V2,A1,A2,normalCalculada);
}
