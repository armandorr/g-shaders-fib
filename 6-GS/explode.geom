#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

in vec4 vfrontColor[];
in vec3 N[];
out vec4 gfrontColor;

uniform mat4 modelViewProjectionMatrix;
uniform float speed = 0.5;
uniform float time;

void main( void )
{
	vec3 normal = vec3(0);
	for( int i = 0 ; i < 3 ; i++ )
	{
		normal += N[i];
	}
	normal /= 3.0;
	mat4 trans = mat4(vec4(1,0,0,0),
			  vec4(0,1,0,0),
			  vec4(0,0,1,0),
			  vec4(speed*time*vec4(normal,0).xyz,1));
	for( int i = 0 ; i < 3 ; i++ )
	{
		gfrontColor = vfrontColor[i];
		vec4 V = trans*gl_in[i].gl_Position;
		//V += speed*time*vec4(normal,0);
		gl_Position = modelViewProjectionMatrix * V;
		EmitVertex();
	}
    	EndPrimitive();
}
