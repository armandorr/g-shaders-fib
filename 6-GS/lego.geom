#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

out vec4 gfrontColor;

uniform mat4 modelViewProjectionMatrix;
uniform float step = 0.1;

in vec4 vfrontColor[];

out vec3 gnormal;
out vec2 gtexCoord;

void main( void )
{
	vec3 used = round((gl_in[0].gl_Position+gl_in[1].gl_Position+gl_in[2].gl_Position).xyz/(3.0*step))*step;
	float s = step/2.0;

	vec4 col = (vfrontColor[0]+vfrontColor[1]+vfrontColor[2])/3.0;
	
	gnormal = vec3(0,0,1); //davant
	gfrontColor = col;
	gtexCoord = vec2(0,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,s),1); EmitVertex(); //0
	gtexCoord = vec2(1,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,s),1); EmitVertex(); //1
	gtexCoord = vec2(0,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,s),1); EmitVertex(); //2
	gtexCoord = vec2(1,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,s),1); EmitVertex(); //3
	EndPrimitive();

	gnormal = vec3(0,1,0); //amunt
	gfrontColor = col;
	gtexCoord = vec2(0,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,s),1); EmitVertex(); //2
	gtexCoord = vec2(1,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,s),1); EmitVertex(); //3
	gtexCoord = vec2(0,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,-s),1); EmitVertex(); //4
	gtexCoord = vec2(1,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,-s),1); EmitVertex(); //5
	EndPrimitive();

	gnormal = vec3(0,0,-1); //darrere
	gfrontColor = col;
	gtexCoord = vec2(0,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,-s),1); EmitVertex(); //4
	gtexCoord = vec2(1,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,-s),1); EmitVertex(); //5
	gtexCoord = vec2(0,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,-s),1); EmitVertex(); //6
	gtexCoord = vec2(1,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,-s),1); EmitVertex(); //7
	EndPrimitive();

	gnormal = vec3(0,-1,0); //avall
	gfrontColor = col;
	gtexCoord = vec2(0,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,s),1); EmitVertex(); //0
	gtexCoord = vec2(1,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,s),1); EmitVertex(); //1
	gtexCoord = vec2(0,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,-s),1); EmitVertex(); //6
	gtexCoord = vec2(1,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,-s),1); EmitVertex(); //7
	EndPrimitive();

	gnormal = vec3(1,0,0); //dreta
	gfrontColor = col;
	gtexCoord = vec2(0,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,s),1); EmitVertex(); //1
	gtexCoord = vec2(1,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,s),1); EmitVertex(); //3
	gtexCoord = vec2(0,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,-s),1); EmitVertex(); //7
	gtexCoord = vec2(1,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,-s),1); EmitVertex(); //5
	EndPrimitive();

	gnormal = vec3(-1,0,0); //esquerra
	gfrontColor = col;
	gtexCoord = vec2(0,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,-s),1); EmitVertex(); //6
	gtexCoord = vec2(1,0); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,s),1); EmitVertex(); //0
	gtexCoord = vec2(0,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,-s),1); EmitVertex(); //4
	gtexCoord = vec2(1,1); gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,s),1); EmitVertex(); //2
	EndPrimitive();
}
