#version 330 core

in vec4 gfrontColor;
out vec4 fragColor;

uniform mat3 normalMatrix;
uniform sampler2D colorMap;

in vec3 gnormal;
in vec2 gtexCoord;

void main()
{
	float dr = distance(gfrontColor.xyz,vec3(1,0,0));
	float dg = distance(gfrontColor.xyz,vec3(0,1,0));
	float db = distance(gfrontColor.xyz,vec3(0,0,1));
	float dc = distance(gfrontColor.xyz,vec3(0,1,1));
	float dy = distance(gfrontColor.xyz,vec3(1,1,0));
	float min;
	if(dr < dg) min = dr;
	else min = dg;
	if(min > db) min = db;
	if(min > dc) min = dc;
	if(min > dy) min = dy;
	float r,g,b;
	if(min == dr){r = 1; g = 0; b = 0;}
	else if(min == dg){r = 0; g = 1; b = 0;}
	else if(min == db){r = 0; g = 0; b = 1;}
	else if(min == dc){r = 0; g = 1; b = 1;}
	else if(min == dy){r = 1; g = 1; b = 0;}
	//Sé que es  pot fer millor pero em volia asegurar de que funcionés bé

	vec4 color = texture(colorMap, gtexCoord); //<- Problema. Quines coordenades de tetura té?
	if(normalize(gnormal) == vec3(0,1,0)) fragColor = color*vec4(r,g,b,1);
	else{
		vec3 normal = normalize(normalMatrix * gnormal);
		fragColor = vec4(r,g,b,1)*normal.z;
	}
}
