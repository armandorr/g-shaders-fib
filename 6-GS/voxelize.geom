#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

out vec4 gfrontColor;

uniform mat4 modelViewProjectionMatrix;
uniform float step = 0.2;
uniform mat3 normalMatrix;

in vec3 vnormal[];

void main( void )
{
	vec3 used = round((gl_in[0].gl_Position+gl_in[1].gl_Position+gl_in[2].gl_Position).xyz/(3.0*step))*step;
	float s = step/2.0;
	vec4 col = vec4(vec3(1),1);

	gfrontColor = col * normalize(normalMatrix * vec3(0,0,1)).z; //davant
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,s),1); EmitVertex(); //0
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,s),1); EmitVertex(); //1
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,s),1); EmitVertex(); //2
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,s),1); EmitVertex(); //3
	EndPrimitive();

	gfrontColor = col * normalize(normalMatrix * vec3(0,1,0)).z; //amunt
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,s),1); EmitVertex(); //2
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,s),1); EmitVertex(); //3
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,-s),1); EmitVertex(); //4
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,-s),1); EmitVertex(); //5
	EndPrimitive();

	gfrontColor = col * normalize(normalMatrix * vec3(0,0,-1)).z; //darrere
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,-s),1); EmitVertex(); //4
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,-s),1); EmitVertex(); //5
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,-s),1); EmitVertex(); //6
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,-s),1); EmitVertex(); //7
	EndPrimitive();

	gfrontColor = col * normalize(normalMatrix * vec3(0,-1,0)).z; //avall
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,s),1); EmitVertex(); //0
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,s),1); EmitVertex(); //1
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,-s),1); EmitVertex(); //6
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,-s),1); EmitVertex(); //7
	EndPrimitive();

	gfrontColor = col * normalize(normalMatrix * vec3(1,0,0)).z; //dreta
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,s),1); EmitVertex(); //1
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,s),1); EmitVertex(); //3
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,-s,-s),1); EmitVertex(); //7
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(s,s,-s),1); EmitVertex(); //5
	EndPrimitive();

	gfrontColor = col * normalize(normalMatrix * vec3(-1,0,0)).z; //esquerra
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,-s),1); EmitVertex(); //6
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,-s,s),1); EmitVertex(); //0
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,-s),1); EmitVertex(); //4
	gl_Position = modelViewProjectionMatrix * vec4(used+vec3(-s,s,s),1); EmitVertex(); //2
	EndPrimitive();
}


