#version 330 core

in vec2 gtexCoord;

out vec4 fragColor;

uniform sampler2D colorMap;

void main()
{
    	vec4 col = texture(colorMap,gtexCoord);
	if(col.a == 1) fragColor = col;
	else discard;
}
