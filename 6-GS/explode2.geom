#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

in vec4 vfrontColor[];
in vec3 N[];
out vec4 gfrontColor;

uniform mat4 modelViewProjectionMatrix;
const float speed = 1.2;
const float angSpeed = 8.0; 
uniform float time;

mat4 rotation3d(vec3 axis, float angle) {
  axis = normalize(axis);
  float s = sin(angle);
  float c = cos(angle);
  float oc = 1.0 - c;

  return mat4(
		oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
    oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
    oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
		0.0,                                0.0,                                0.0,                                1.0
	);
}

void main( void )
{
	vec3 normal = vec3(0);
	vec4 bari = vec4(0); 
	for( int i = 0 ; i < 3 ; i++ )
	{
		normal += N[i];
		bari += gl_in[i].gl_Position;
	}
	normal /= 3.0;
	bari   /= 3.0;
	float angle = angSpeed*time;
	mat4 rot = rotation3d(vec3(bari.xyz),angle);
	mat4 trans = mat4(vec4(1,0,0,0),
			  vec4(0,1,0,0),
			  vec4(0,0,1,0),
			  vec4(speed*time*vec4(normal,0).xyz,1));
	for( int i = 0 ; i < 3 ; i++ )
	{
		gfrontColor = vfrontColor[i];
		vec4 V = trans*rot*gl_in[i].gl_Position;
		gl_Position = modelViewProjectionMatrix * V;
		EmitVertex();
	}
    	EndPrimitive();
}

