#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

in vec4 vfrontColor[];
out vec4 gfrontColor;

uniform mat4 projectionMatrix;

const float areamax = 0.0005;

void main( void )
{
	vec3 V0 = (gl_in[0].gl_Position).xyz;
	vec3 V1 = (gl_in[1].gl_Position).xyz;
	vec3 V2 = (gl_in[2].gl_Position).xyz;

	vec3 U = V1-V0;
	vec3 V = V2-V0;
	vec3 UxV = cross(U,V);
	float value = (length(UxV)/2.0)/areamax;
	vec3 col = mix(vec3(1,0,0),vec3(1,1,0),fract(value));
	if(value >= 1.0) col = vec3(1,1,0);
	gfrontColor = vec4(col,1);

	gl_Position = projectionMatrix*vec4(V0,1); EmitVertex();
	gl_Position = projectionMatrix*vec4(V1,1); EmitVertex();
	gl_Position = projectionMatrix*vec4(V2,1); EmitVertex();
    	EndPrimitive();
}
