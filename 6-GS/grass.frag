#version 330 core

in vec3 gnormal;
in vec3 gPos;
in float D;
out vec4 fragColor;

uniform sampler2D grass_top0, grass_side1;
uniform mat3 normalMatrix; 

void main()
{
	if(gnormal.z == 0){
		vec2 tex = vec2(4*(gPos.x - gPos.y), 1.0 - gPos.z/D);
		vec4 col = texture(grass_side1,tex);
		if(col.a < 0.1) discard;
		else fragColor = col;
	}
	else fragColor = texture(grass_top0,4*gPos.xy);
}
