#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

uniform mat4 modelViewProjectionMatrix;
uniform vec3 boundingBoxMin;
uniform vec3 boundingBoxMax;

in vec4 vfrontColor[];
out vec4 gfrontColor;

void main( void )
{
	for( int i = 0 ; i < 3 ; i++ )
	{
		gfrontColor = vfrontColor[i];
		gl_Position = modelViewProjectionMatrix * gl_in[i].gl_Position;
		EmitVertex();
	}
    	EndPrimitive();

	gfrontColor = vec4(0);
	for( int i = 0 ; i < 3 ; i++ )
	{
		vec4 V = gl_in[i].gl_Position;
		V.y = 0.0;
		V.y += boundingBoxMin.y;
		gl_Position =  modelViewProjectionMatrix * V;
		EmitVertex();
	}
    	EndPrimitive();
	gfrontColor = vec4(0,1,1,0);
	vec3 centreBase = 0.5*(boundingBoxMin+boundingBoxMax);
	float R = distance(boundingBoxMin, boundingBoxMax)/2.0;
	if(gl_PrimitiveIDIn == 0){
		gl_Position = modelViewProjectionMatrix * vec4(centreBase.x-R,boundingBoxMin.y-0.01,centreBase.z+R,1); EmitVertex();
		gl_Position = modelViewProjectionMatrix * vec4(centreBase.x+R,boundingBoxMin.y-0.01,centreBase.z+R,1); EmitVertex();
		gl_Position = modelViewProjectionMatrix * vec4(centreBase.x-R,boundingBoxMin.y-0.01,centreBase.z-R,1); EmitVertex();
		gl_Position = modelViewProjectionMatrix * vec4(centreBase.x+R,boundingBoxMin.y-0.01,centreBase.z-R,1); EmitVertex();	
	}
	EndPrimitive();
}
