#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

in vec4 vfrontColor[];
in vec3 vnormal[];

out vec4 gfrontColor;

uniform mat4 projectionMatrix;
uniform float disp = 0.05;

vec3 getNormal(vec3 v1, vec3 v2, vec3 v3) {
  	return normalize(cross(v2-v1, v3-v1))
}

void emitVertex(vec3 V){
	gl_Position = projectionMatrix * vec4(V,1);
	EmitVertex();
}

void emitStrip3(vec3 V0, vec4 colV0, vec3 V1, vec4 colV1, vec3 V2, vec4 colV2){
	gfrontColor = colV0;
	emitVertex(V0);
	gfrontColor = colV1;
	emitVertex(V1);
	gfrontColor = colV2;
	emitVertex(V2);
	EndPrimitive();
}

void main( void )
{
	vec3 N  = normalize((vnormal[0]+vnormal[1]+vnormal[2])/3.0);
	vec3 V0 = (gl_in[0].gl_Position).xyz;
	vec3 V1 = (gl_in[1].gl_Position).xyz;
	vec3 V2 = (gl_in[2].gl_Position).xyz;
	vec4 colV0 = vfrontColor[0];
	vec4 colV1 = vfrontColor[1];
	vec4 colV2 = vfrontColor[2];
	vec3 B  = (V0+V1+V2)/3.0 + disp*N;
	vec4 colB = vec4(1.0);
	
	emitStrip3(V0,colV0,B,colB,V2,colV2);
	emitStrip3(V1,colV1,B,colB,V2,colV2);
	emitStrip3(V0,colV0,B,colB,V1,colV1);
}
