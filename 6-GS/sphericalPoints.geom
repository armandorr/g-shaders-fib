#version 330 core
        
layout(triangles) in;
layout(triangle_strip, max_vertices = 36) out;

uniform float side = 0.1;
uniform mat4 projectionMatrix;

out vec2 gtexCoord;

void emitVertex(vec3 V){
	gl_Position = projectionMatrix * vec4(V,1);
	EmitVertex();
}

void emitStrip4(vec3 V0, vec3 V1, vec3 V2, vec3 V3){
	gtexCoord = vec2(0,0);
	emitVertex(V0);
	gtexCoord = vec2(1,0);
	emitVertex(V1);
	gtexCoord = vec2(0,1);
	emitVertex(V2);
	gtexCoord = vec2(1,1);
	emitVertex(V3);
	EndPrimitive();
}

void main( void )
{
	vec3 centre = (gl_in[0].gl_Position).xyz;
	float meitat = side/2;
	vec3 V0 = centre + vec3(-meitat,-meitat,0);
	vec3 V1 = centre + vec3(meitat,-meitat,0);	
	vec3 V2 = centre + vec3(-meitat,meitat,0);
	vec3 V3 = centre + vec3(meitat,meitat,0);

	emitStrip4(V0,V1,V2,V3);
}
