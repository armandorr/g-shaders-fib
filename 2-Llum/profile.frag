#version 330 core

in vec4 frontColor;
out vec4 fragColor;

in vec3 N;
in vec3 vert;

uniform float epsilon = 0.1;
uniform float light = 0.5;
uniform mat4 viewMatrixInverse;

vec3 grocFosc = vec3(0.7,0.6,0.0);

void main()
{
    if(abs(dot(normalize(N),-normalize(vec3(vert))))<epsilon)
    	fragColor = vec4(grocFosc,1.0);
    else fragColor = frontColor*light*N.z;
}
