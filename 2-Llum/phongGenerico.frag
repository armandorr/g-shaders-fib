#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform vec4 lightAmbient; //ia
uniform vec4 lightDiffuse; //id
uniform vec4 lightSpecular; //is
uniform vec4 lightPosition;
 
uniform vec4 matAmbient; //ka
uniform vec4 matDiffuse; //kd
uniform vec4 matSpecular; //ks
uniform float matShininess; //s

uniform mat4 modelMatrix;
uniform mat4 modelMatrixInverse;
uniform mat4 viewMatrix;
uniform mat4 viewMatrixInverse;
uniform mat4 modelViewMatrix;
uniform mat4 modelViewMatrixInverse;
uniform mat3 normalMatrix;
uniform mat3 normalMatrixInverse;

in vec3 V;
in vec3 N;

uniform int num = 1;

vec4 phong(vec3 normal, vec3 vertex, vec3 Lposition)
{
	normal=normalize(normal); vertex=normalize(vertex);
	Lposition=normalize(Lposition);
	
	vec3 L = normalize(Lposition.xyz-vertex.xyz);
	
	vec3 R = normalize( 2.0*dot(N,L)*N-L );
	float NdotL = max( 0.0, dot( N,L ) );
	
	//object
	//vertex = modelviewMatrixInverse[3].xyz-vertex; 
	//word
	//vertex = viewMatrixInverse[3].xyz-vertex; 
	//eye
	//vertex = -vertex;
	
	float RdotV = max( 0.0, dot( R,V ) );
	float Idiff = NdotL;
	float Ispec = 0;
	if (NdotL>0) Ispec=pow( RdotV, matShininess );
	
	return matAmbient * lightAmbient + matDiffuse * lightDiffuse * Idiff+ matSpecular * lightSpecular * Ispec;
}

void main()
{
    vec3 norm;
    vec3 vert;
    vec3 L;
    vec4 light = vec4(0);
	
	if(num == 1){ //object
        norm = N;
		vert = V;
        L = (modelViewMatrixInverse*light).xyz;
    }
    if(num == 2){ //world
        norm = inverse(transpose(modelMatrix)*N;
		vert = (modelMatrix*vec4(V,1.0)).xyz;
        L = (viewMatrixInverse*light).xyz;
    }
    if(num == 3){ //eye
        norm = normalMatrix*norm;
		vert = (modelViewMatrix*vec4(V,1.0)).xyz;
		L = light.xyz;
    }
	
    fragColor = phong(norm,vert,L);
}
