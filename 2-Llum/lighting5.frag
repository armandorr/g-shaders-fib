#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform vec4 lightAmbient; //ia
uniform vec4 lightDiffuse; //id
uniform vec4 lightSpecular; //is
uniform vec4 lightPosition;
 
uniform vec4 matAmbient; //ka
uniform vec4 matDiffuse; //kd
uniform vec4 matSpecular; //ks
uniform float matShininess; //s

uniform bool world;

uniform mat4 modelViewMatrixInverse;
uniform mat4 modelViewMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 viewMatrixInverse;
uniform mat3 normalMatrix;
uniform mat3 normalMatrixInverse;

in vec3 V;
in vec3 norm;

vec4 phong(vec3 N, vec3 V, vec3 L)
{
	N=normalize(N); V=normalize(V); L=normalize(L);
	vec3 R = normalize( 2.0*dot(N,L)*N-L );
	float NdotL = max( 0.0, dot( N,L ) );
	float RdotV = max( 0.0, dot( R,V ) );
	float Idiff = NdotL;
	float Ispec = 0;
	if (NdotL>0) Ispec=pow( RdotV, matShininess );
	return matAmbient * lightAmbient + matDiffuse * lightDiffuse * Idiff+ matSpecular * lightSpecular * Ispec;
}

void main()
{
    vec3 L;
    vec3 vert;
    vec3 N;
    
  if(world){ //object or world
        N = norm;
        L = (viewMatrixInverse*lightPosition).xyz - V;
        vert = viewMatrixInverse[3].xyz-V; 
   }
    
    else{
	vec3 VE = (modelViewMatrix*vec4(V,1.0)).xyz;
        N = normalMatrix*norm;
	L = lightPosition.xyz-VE;
        vert = -VE.xyz;
    }
    fragColor = light(N,vert,L);
}
