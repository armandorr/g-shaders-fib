#version 330 core

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 color;
layout (location = 3) in vec2 texCoord;

out vec4 frontColor;
out vec2 vtexCoord;

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat3 normalMatrix;

uniform vec4 lightAmbient; //ia
uniform vec4 lightDiffuse; //id
uniform vec4 lightSpecular; //is
uniform vec4 lightPosition;
 
uniform vec4 matAmbient; //ka
uniform vec4 matDiffuse; //kd
uniform vec4 matSpecular; //ks
uniform float matShininess; //s

vec4 phong(vec3 N, vec4 vert){
    vec3 L = normalize(lightPosition.xyz-vert.xyz);
    vec4 col1 = matAmbient*lightAmbient;
    vec4 col2 = matDiffuse*lightDiffuse * max(0.0, dot(N, L));

    if(dot(N,L) < 0) return col1+col2;

    vec3 R = normalize(2 * dot(N,L)*N -L);

    vec3 V = normalize(-vert.xyz);

    vec4 col3 = matSpecular*lightSpecular * pow(max(0.0, dot(R, V)),matShininess);

    return col1+col2+col3;
}

void main()
{
    vec3 N = normalize(normalMatrix * normal);
    vec4 V = modelViewMatrix*vec4(vertex,1.0);
    frontColor = phong(N,V);
    vtexCoord = texCoord;
    gl_Position = modelViewProjectionMatrix * vec4(vertex, 1.0);
}
