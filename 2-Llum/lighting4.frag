#version 330 core

in vec4 frontColor;
out vec4 fragColor;

uniform vec4 lightAmbient; //ia
uniform vec4 lightDiffuse; //id
uniform vec4 lightSpecular; //is
uniform vec4 lightPosition;
 
uniform vec4 matAmbient; //ka
uniform vec4 matDiffuse; //kd
uniform vec4 matSpecular; //ks
uniform float matShininess; //s

in vec4 vert;
in vec3 norm;

vec4 phong(){
    vec3 L = normalize(lightPosition.xyz-vert.xyz);
    vec4 col1 = matAmbient*lightAmbient;
    vec4 col2 = matDiffuse*lightDiffuse * max(0.0, dot(norm, L));
    if(dot(norm,L) < 0) return col1+col2;

    vec3 R = normalize(2 * dot(norm,L)*norm -L);
    vec3 V = normalize(-vert.xyz);

    vec4 col3 = matSpecular*lightSpecular * pow(max(0.0, dot(R, V)),matShininess);

    return col1+col2+col3;
}

void main()
{
    fragColor = phong();
}
